const Account = require('../models/Account')
const accountController = {
  accList: [
    {
      id: 1,
      name: 'PiggyBlue',
      type: 'Deposit',
      amount: 200,
      balance: 500
    },
    {
      id: 2,
      name: 'PiggyBlue',
      type: 'Withdraw',
      amount: 100,
      balance: 500
    }
  ],
  lastId: 3,

  async addAccount (req, res, next) {
    const payload = req.body
    console.log(payload)
    const account = new Account(payload)
    try {
      await account.save()
      res.json(account)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateAccount (req, res, next) {
    const payload = req.body
    try {
      const account = await Account.updateOne({ _id: payload._id }, payload)
      res.json(account)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteAccount (req, res, next) {
    const { id } = req.params
    try {
      const account = await Account.deleteOne({ _id: id })
      res.json(account)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAccounts (req, res, next) {
    try {
      const accounts = await Account.find({})
      res.json(accounts)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAccount (req, res, next) {
    const { id } = req.params
    try {
      const account = await Account.findById(id)
      res.json(account)
    } catch (err) {
      res.stsatus(500).send(err)
    }
  }
}

module.exports = accountController
