const express = require('express')
const router = express.Router()
const accountsController = require('../controller/AccountsController')

router.get('/', accountsController.getAccounts)

router.get('/:id', accountsController.getAccount)

router.post('/', accountsController.addAccount)

router.put('/', accountsController.updateAccount)

router.delete('/:id', accountsController.deleteAccount)

module.exports = router
