const mongoose = require('mongoose')
const Schema = mongoose.Schema

const accountSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3
  },
  type: {
    type: String,
    enum: ['Deposit', 'Withdraw']
  },
  amount: {
    type: Number,
    min: 1
  },
  balance: {
    type: Number,
    min: 0
  },
  date: Date
})

module.exports = mongoose.model('Account', accountSchema)
