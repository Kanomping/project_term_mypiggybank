const dbHandler = require('./db-handler')
const Account = require('../models/Account')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const accountComplete1 = {
  name: 'PiggyBlue',
  type: 'Deposit',
  amount: 200
}

const accountComplete2 = {
  name: 'PiggyBlue',
  type: 'Withdraw',
  amount: 200
}

const accountErrorNameEmpty = {
  name: '',
  type: 'Deposit',
  amount: 200
}

const accountErrorName2Alpha = {
  name: 'Ko',
  type: 'Deposit',
  amount: 200
}

const accountErrorType = {
  name: 'PiggyBlue',
  type: 'x',
  amount: 200
}
const accountErrorAmount = {
  name: 'PiggyBlue',
  type: 'Depposit',
  amount: 0
}
describe('Account', () => {
  it('Can add new account type withdraw', async () => {
    let error = null
    try {
      const account = new Account(accountComplete1)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('Can add new account type withdraw', async () => {
    let error = null
    try {
      const account = new Account(accountComplete2)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('Can not add new account because name is empty', async () => {
    let error = null
    try {
      const account = new Account(accountErrorNameEmpty)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Can not add new account because name is 2 ', async () => {
    let error = null
    try {
      const account = new Account(accountErrorName2Alpha)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Can not add new account because type invalid ', async () => {
    let error = null
    try {
      const account = new Account(accountErrorType)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('Can not add new account because amount is 0 ', async () => {
    let error = null
    try {
      const account = new Account(accountErrorAmount)
      await account.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
